from scipy.spatial import cKDTree
from collections import defaultdict
import readgadgetdata
import numpy as np
import itertools
import time
import h5py
import sys


def read_sim(snapshot, PATH, llcoeff):
    """ Reads in gadget-2 simulation data and computes the host halo linking length. (For more information see Docs)

    :param snapshot: The snapshot ID as a string (e.g. '061')
    :param PATH: The filepath to the directory containing the simulation data.
    :param llcoeff: The host halo linking length coefficient.

    :return: pid: An array containing the particle IDs.
             pos: An array of the particle position vectors.
             vel: An array of the particle velocity vectors.
             npart: The number of particles used in the simulation.
             boxsize: The length of the simulation box along a single axis.
             redshift: The redshift of the current snapshot.
             t: The elapsed time of the current snapshot.
             rhocrit: The critical density at the current snapshot.
             pmass: The mass of a dark matter particle.
             h: 'Little h', The hubble parameter parametrisation.
             linkl: The linking length.

    """

    # =============== Load Simulation Data ===============

    # Load snapshot data from gadget-2 file *** Note: will need to be changed for use with other simulations data ***
    snap = readgadgetdata.readsnapshot(snapshot, PATH)
    pid, pos, vel = snap[0:3]  # pid=particle ID, pos=all particle's position, vel=all particle's velocity
    head = snap[3:]  # header values
    npart = head[0]  # number of particles in simulation
    boxsize = head[3]  # simulation box length(/size) along each axis
    redshift = head[1]
    t = head[2]  # elapsed time of the snapshot
    rhocrit = head[4]  # Critical density
    pmass = head[5]  # Particle mass
    h = head[6]  # 'little h' (hubble parameter parametrisation)

    # =============== Compute Linking Length ===============

    # Compute the mean separation
    mean_sep = boxsize / npart**(1./3.)

    # Compute the linking length for host halos
    linkl = llcoeff * mean_sep

    return pid, pos, vel, npart, boxsize, redshift, t, rhocrit, pmass, h, linkl


def find_halos(pos, npart, boxsize, batchsize, linkl, debug_npart):
    """ A function which creates a KD-Tree using scipy.CKDTree and queries it to find particles
    neighbours within a linking length. From This neighbour information particles are assigned
    halo IDs and then returned.

    :param pos: The particle position vectors array.
    :param npart: The number of particles in the simulation.
    :param boxsize: The length of the simulation box along one axis.
    :param batchsize: The batchsize for each query to the KD-Tree (see Docs for more information).
    :param linkl: The linking length.
    :param debug_npart: Number of particles to sort during debugging if required.

    :return: part_haloids: The array of halo IDs assigned to each particle (where the index is the particle ID)
             assigned_parts: A dictionary containing the particle IDs assigned to each halo.
             final_halo_ids: An array of final halo IDs (where the index is the initial halo ID and value is the
             final halo ID.
             query_func: The tree query object assigned to a variable.
    """

    # =============== Initialise The Halo Finder Variables/Arrays and The KD-Tree ===============

    # Initialise the arrays and dictionaries for storing halo data
    part_haloids = np.full(npart, -1, dtype=int)  # halo ID containing each particle
    assigned_parts = defaultdict(set)  # dictionary to store the particles in a particular halo
    # A dictionary where each key is an initial halo ID and the item is the halo IDs it has been linked with
    linked_halos_dict = defaultdict(set)
    final_halo_ids = np.full(npart, -1, dtype=int)  # final halo ID of linked halos (index is initial halo ID)

    # Initialise the halo ID counter (IDs start at 0)
    ihaloid = -1

    # Build the kd tree with the boxsize argument providing 'wrapping' due to periodic boundaries
    # *** Note: Contrary to CKDTree documentation compact_nodes=False and balanced_tree=False results in
    # faster queries (documentation recommends compact_nodes=True and balanced_tree=True)***
    tree = cKDTree(pos, leafsize=16, compact_nodes=False, balanced_tree=False, boxsize=[boxsize, boxsize, boxsize])

    # Assign the query object to a variable to save time on repeated calls
    query_func = tree.query_ball_point

    # Overwrite npart for debugging purposes if called in arguments
    if debug_npart is not None:
        npart = debug_npart

    # =============== Assign Particles To Initial Halos ===============

    # Define an array of limits for looping defined by the batchsize
    limits = np.linspace(0, npart, int(npart/batchsize), dtype=int)

    # Loop over particle batches
    for ind, limit in enumerate(limits[:-1]):

        # Print progress
        print('Processed: {x}/{y}'.format(x=limit, y=npart))

        # Remove already assigned particles from the query particles to save time avoiding double checks
        query_parts_poss = pos[limit:limits[ind+1]]

        # Query the tree in batches of 1000000 for speed returning a list of lists
        query = query_func(query_parts_poss, r=linkl, n_jobs=-1)

        # Loop through query results assigning initial halo IDs
        for query_part_inds in iter(query):

            # Convert the particle index list to an array for ease of use
            query_part_inds = np.array(query_part_inds, copy=False, dtype=int)

            # Assert that the query particle is returned by the tree query. Otherwise the program fails
            assert query_part_inds.size != 0, 'Must always return particle that you are sitting on'

            # Find only the particles not already in a halo
            new_parts = query_part_inds[np.where(part_haloids[query_part_inds] == -1)]

            # If only one particle is returned by the query and it is new it is a 'single particle halo'
            if new_parts.size == query_part_inds.size == 1:

                # Assign the 'single particle halo' halo ID to the particle
                part_haloids[new_parts] = -2

            # If all particles are new increment the halo ID and assign a new halo
            elif new_parts.size == query_part_inds.size:

                # Increment the halo ID by 1 (initialising a new halo)
                ihaloid += 1

                # Assign the new halo ID to the particles
                part_haloids[new_parts] = ihaloid
                assigned_parts[ihaloid] = set(new_parts)

                # Assign the final halo ID to be the newly assigned halo ID
                final_halo_ids[ihaloid] = ihaloid
                linked_halos_dict[ihaloid] = {ihaloid}

            else:

                # ===== Get the 'final halo ID value' =====

                # Extract the IDs of halos returned by the query
                contained_halos = part_haloids[query_part_inds]

                # Get only the unique halo IDs
                uni_cont_halos = np.unique(contained_halos)

                # Assure no single particle halos are included in the query results
                assert any(uni_cont_halos != -2), 'Single particle halos should never be found'

                # Remove any unassigned halos
                uni_cont_halos = uni_cont_halos[np.where(uni_cont_halos != -1)]

                # If there is only one halo ID returned avoid the slower code to combine IDs
                if uni_cont_halos.size == 1:

                    # Get the list of linked halos linked to the current halo from the linked halo dictionary
                    linked_halos = linked_halos_dict[uni_cont_halos[0]]

                else:

                    # Get all the linked halos from dictionary so as to not miss out any halos IDs that are linked
                    # but not returned by this particular query
                    linked_halos_set = set()  # initialise linked halo set
                    linked_halos = linked_halos_set.union(*[linked_halos_dict.get(halo) for halo in uni_cont_halos])

                # Find the minimum halo ID to make the final halo ID
                final_ID = min(linked_halos)

                # Assign the linked halos to all the entries in the linked halos dictionary
                linked_halos_dict.update(dict.fromkeys(list(linked_halos), linked_halos))

                # Assign the final halo ID array entries
                final_halo_ids[list(linked_halos)] = final_ID

                # Assign new particles to the particle halo IDs array with the final ID
                part_haloids[new_parts] = final_ID

                # Assign the new particles to the final ID in halo dictionary entry
                assigned_parts[final_ID].update(new_parts)

    # =============== Reassign All Halos To Their Final Halo ID ===============

    # Loop over initial halo IDs reassigning them to the final halo ID
    for halo_id in assigned_parts.keys():

        # Extract the final halo value
        final_ID = final_halo_ids[halo_id]

        # Assign this final ID to all the particles in the initial halo ID
        part_haloids[list(assigned_parts[halo_id])] = final_ID
        assigned_parts[final_ID].update(assigned_parts[halo_id])

    print('Assignment Complete')

    return part_haloids, assigned_parts, final_halo_ids, query_func


def halo_prit_stick(part_haloids, assigned_parts, query_func, pos, linkl):
    """ A function which re-queries the found halos to make sure all halos have been correctly assigned.
    If halos are found to have new particles then the halos containing all these particles are 'glued'. This
    is a debugging function and is unnecessary unless results require double checking for validity.

    :param part_haloids: The array of halo IDs assigned to each particle (where the index is the particle ID)
    :param assigned_parts: A dictionary containing the particle IDs assigned to each halo.
    :param query_func: The tree query object assigned to a variable.
    :param pos: The particle position vectors array.
    :param linkl: The linking length.

    :return: part_haloids: The array of halo IDs assigned to each particle (where the index is the particle ID)
    """

    # =============== Query All Halos To Test For Any Remaining Split Halos ===============

    # Find the individual halo IDs after reassignment from the 'final ID' array
    gluing_list, counts = np.unique(part_haloids, return_counts=True)

    # Convert to a list removing the single particle halo ID (-2)
    gluing_list = list(gluing_list[np.where(gluing_list != -2)])

    # Find the maximum ID to use as a starting point for reassignment of halo IDs while iterating to
    # avoid confusion by reassigning existing IDs (new IDs > old Ids)
    new_id = max(gluing_list)

    # Define the length of the unique halo IDs to track progress
    org_len = len(gluing_list)

    # Initialise progress counter
    progress = -1

    # Loop until there are no halos which require testing
    while len(gluing_list) != 0:

        # Define the halo particles for testing from the particles in the halo dictionary
        test_parts = list(assigned_parts[gluing_list[0]])

        # Query the test particles
        query = query_func(pos[test_parts], linkl, n_jobs=-1)

        # Convert list of lists from query to a single list
        query_parts = np.unique(list(itertools.chain.from_iterable(query)))

        # Check whether new particles were found (if so len(query_parts) > len(test_parts))
        if len(test_parts) != len(query_parts):

            # Increment the new ID
            new_id += 1

            glued_parts = query_parts

            # Assign the new halo ID
            part_haloids[glued_parts] = new_id

            # Add the new ID to the testing list
            gluing_list.append(new_id)

            # Remove the tested halo from the assigned particle dictionary
            assigned_parts.pop(gluing_list[0])

            # Create a new entry for the new ID
            assigned_parts[new_id] = glued_parts

            # Print progress
            previous_progress = progress
            progress = int((org_len-len(gluing_list))/org_len * 100)
            if progress != previous_progress:
                print('Gluing progress: ', progress, '%')

        # Remove the old halo ID from the list for gluing
        gluing_list.remove(gluing_list[0])

    return part_haloids


def find_subhalos(halo_pos, sub_llcoeff, boxsize, npart):
    """ A function that finds subhalos within host halos by applying the same KD-Tree algorithm at a
    higher overdensity.

    :param halo_pos: The position vectors of particles within the host halo.
    :param sub_llcoeff: The linking length coefficient used to define a subhalo.
    :param boxsize: The length of the simulation box along one axis.
    :param npart: The number of particles in the simulation.

    :return: part_subhaloids: The array of subhalo IDs assigned to each particle in the host halo
             (where the index is the particle ID).
             assignedsub_parts: A dictionary containing the particle IDs assigned to each subhalo.
    """

    # =============== Compute The Subhalo Linking Length  ===============

    # Compute the mean separation
    mean_sep = boxsize / npart**(1./3.)

    # Compute the linking length for subhalos
    sub_linkl = sub_llcoeff * mean_sep

    # =============== Initialise The Halo Finder Variables/Arrays and The KD-Tree ===============

    # Initialise arrays and dictionaries for storing subhalo data
    part_subhaloids = np.full(halo_pos.shape[0], -1, dtype=int)  # subhalo ID of the halo each particle is in
    assignedsub_parts = defaultdict(set)  # Dictionary to store the particles in a particular subhalo
    # A dictionary where each key is an initial subhalo ID and the item is the subhalo IDs it has been linked with
    linked_subhalos_dict = defaultdict(set)
    # Final subhalo ID of linked halos (index is initial subhalo ID)
    final_subhalo_ids = np.full(halo_pos.shape[0], -1, dtype=int)

    # Initialise subhalo ID counter (IDs start at 0)
    isubhaloid = -1

    # Build the kd tree
    # *** Note: Contrary to CKDTree documentation compact_nodes=False and balanced_tree=False results in
    # faster queries (documentation recommends compact_nodes=True and balanced_tree=True)***
    tree = cKDTree(halo_pos, leafsize=16, compact_nodes=False, balanced_tree=False)

    # Assign the query object to a variable to save time on repeated calls
    query_func = tree.query_ball_point

    # Query the tree foir all particles in this halo
    query = query_func(halo_pos, r=sub_linkl, n_jobs=-1)

    # ===================== Sort Through The Results Assigning Subhalos =====================

    # Loop through query results
    for query_part_inds in iter(query):

        # Convert the particle index list to an array for ease of use.
        query_part_inds = np.array(query_part_inds, copy=False, dtype=int)

        # Assert that the query particle is returned by the tree query. Otherwise the program fails
        assert query_part_inds.size != 0, 'Must always return particle that you are sitting on'

        # Find only the particles not already in a halo
        new_parts = query_part_inds[np.where(part_subhaloids[query_part_inds] == -1)]

        # If only one particle is returned by the query and it is new it is a 'single particle subhalo'
        if new_parts.size == query_part_inds.size == 1:

            # Assign the 'single particle subhalo' subhalo ID to the particle
            part_subhaloids[new_parts] = -2

        # If all particles are new increment the subhalo ID and assign a new subhalo ID
        elif new_parts.size == query_part_inds.size:

            # Increment the subhalo ID by 1 (initialise new halo)
            isubhaloid += 1

            # Assign the subhalo ID to the particles
            part_subhaloids[new_parts] = isubhaloid
            assignedsub_parts[isubhaloid] = set(new_parts)

            # Assign the final subhalo ID to be the newly assigned subhalo ID
            final_subhalo_ids[isubhaloid] = isubhaloid
            linked_subhalos_dict[isubhaloid] = {isubhaloid}

        else:

            # ===== Get the 'final subhalo ID value' =====

            # Extract the IDs of subhalos returned by the query
            contained_subhalos = part_subhaloids[query_part_inds]

            # Return only the unique subhalo IDs
            uni_cont_subhalos = np.unique(contained_subhalos)

            # Assure no single particles are returned by the query
            assert any(uni_cont_subhalos != -2), 'Single particle halos should never be found'

            # Remove any unassigned subhalos
            uni_cont_subhalos = uni_cont_subhalos[np.where(uni_cont_subhalos != -1)]

            # If there is only one subhalo ID returned avoid the slower code to combine IDs
            if uni_cont_subhalos.size == 1:

                # Get the list of linked subhalos linked to the current subhalo from the linked subhalo dictionary
                linked_subhalos = linked_subhalos_dict[uni_cont_subhalos[0]]

            else:

                # Get all linked subhalos from the dictionary so as to not miss out any subhalos IDs that are linked
                # but not returned by this particular query
                linked_subhalos_set = set()  # initialise linked subhalo set
                linked_subhalos = linked_subhalos_set.union(*[linked_subhalos_dict.get(subhalo)
                                                              for subhalo in uni_cont_subhalos])

            # Find the minimum subhalo ID to make the final subhalo ID
            final_ID = min(linked_subhalos)

            # Assign the linked subhalos to all the entries in the linked subhalos dict
            linked_subhalos_dict.update(dict.fromkeys(list(linked_subhalos), linked_subhalos))

            # Assign the final subhalo array
            final_subhalo_ids[list(linked_subhalos)] = final_ID

            # Assign new parts to the subhalo IDs with the final ID
            part_subhaloids[new_parts] = final_ID

            # Assign the new particles to the final ID particles in subhalo dictionary entry
            assignedsub_parts[final_ID].update(new_parts)

    # =============== Reassign All Subhalos To Their Final Subhalo ID ===============

    # Loop over initial subhalo IDs reassigning them to the final subhalo ID
    for subhalo_id in assignedsub_parts.keys():

        # Extract the final subhalo value
        final_ID = final_subhalo_ids[subhalo_id]

        # Assign this final ID to all the particles in the initial subhalo ID
        part_subhaloids[list(assignedsub_parts[subhalo_id])] = final_ID
        assignedsub_parts[final_ID].update(assignedsub_parts[subhalo_id])

    return part_subhaloids, assignedsub_parts


def hosthalofinder(snapshot, llcoeff=0.2, sub_llcoeff=0.1, gadgetpath='snapshotdata/snapdir_', batchsize=1000000,
                   debug_npart=None, Glue=False):
    """ Run the halo finder, sort the output results, find subhalos and save to a HDF5 file.

    :param snapshot: The snapshot ID.
    :param llcoeff: The host halo linking length coefficient.
    :param sub_llcoeff: The subhalo linking length coefficient.
    :param gadgetpath: The filepath to the gadget simulation data.
    :param batchsize: The number of particle to be queried at one time.
    :param debug_npart: The number of particles to run the program on when debugging.
    :param Glue: Boolean, True if result checking is required.

    :return: None
    """

    # =============== Load Simulation Data, Compute The Linking Length And Sort Simulation Data ===============

    pid, pos, vel, npart, boxsize, redshift, t, rhocrit, pmass, h, linkl = read_sim(snapshot,
                                                                                    PATH=gadgetpath, llcoeff=llcoeff)

    # Sort the simulation data arrays by the particle ID
    sinds = pid.argsort()
    pid = pid[sinds]
    pos = pos[sinds, :]
    vel = vel[sinds, :]

    # =============== Run The Halo Finder And Reduce The Output ===============

    # Run the halo finder for this snapshot and assign the results to the relevant variables
    assin_start = time.time()
    part_haloids, assigned_parts, final_halo_ids, query_func = find_halos(pos, npart, boxsize, batchsize,
                                                                          linkl, debug_npart)
    print('Initial assignment: ', time.time()-assin_start)

    # Find the halos with 10 or more particles by finding the unique IDs in the particle
    # halo ids array and finding those IDs that are assigned to 10 or more particles
    unique, counts = np.unique(part_haloids, return_counts=True)
    unique_haloids = unique[np.where(counts >= 10)]

    # Remove the null -2 value for single particle halos
    unique_haloids = unique_haloids[np.where(unique_haloids != -2)]

    # Print the number of halos found by the halo finder in >10, >100, >1000, >10000 criteria
    print(unique_haloids.size, 'halos found with 10 or more particles')
    print(unique[np.where(counts >= 20)].size - 1, 'halos found with 20 or more particles')
    print(unique[np.where(counts >= 100)].size - 1, 'halos found with 100 or more particles')
    print(unique[np.where(counts >= 1000)].size - 1, 'halos found with 1000 or more particles')
    print(unique[np.where(counts >= 10000)].size - 1, 'halos found with 10000 or more particles')

    # If gluing is required apply the gluing algorithm and print results to compare to the result from find_halos
    if Glue:

        # Run the gluing algorithm to check the halos returned by find_neighbours
        glue_start = time.time()
        part_haloids = halo_prit_stick(part_haloids, assigned_parts, query_func, pos, linkl)
        print('Gluing: ', time.time()-glue_start)

        # Find the glued halos with 10 or more particles as before
        unique, counts = np.unique(part_haloids, return_counts=True)
        unique_haloids = unique[np.where(counts >= 10)]

        # Remove the null -2 value for single particle halos
        unique_haloids = unique_haloids[np.where(unique_haloids != -2)]

        # Print the number of halos found after gluing in >10, >100, >1000, >10000 criteria
        print(unique_haloids.size, 'halos found with 10 or more particles')
        print(unique[np.where(counts >= 20)].size - 1, 'halos found with 20 or more particles')
        print(unique[np.where(counts >= 100)].size - 1, 'halos found with 100 or more particles')
        print(unique[np.where(counts >= 1000)].size - 1, 'halos found with 1000 or more particles')
        print(unique[np.where(counts >= 10000)].size - 1, 'halos found with 10000 or more particles')

    # Initialise the counters for the number of subhalos in a mass bin
    plus10subs = 0
    plus20subs = 0
    plus100subs = 0
    plus1000subs = 0
    plus10000subs = 0

    # =============== Assign The Halo Data To A HDF5 File ===============

    # Create the root group
    snap = h5py.File('halo_snapshots/halos_' + str(snapshot) + '.hdf5', 'w')

    # Assign simulation attributes to the root of the z=0 snapshot
    if snapshot == '061':
        # *** Note: Potentially groups can be moved to a master root group with simulation attributes in the root ****
        snap.attrs['snap_nPart'] = npart  # number of particles in the simulation
        snap.attrs['boxsize'] = boxsize  # box length along each axis
        snap.attrs['partMass'] = pmass  # particle mass
        snap.attrs['h'] = h  # 'little h' (hubble constant parametrisation)

    # Assign snapshot attributes
    snap.attrs['linkingLength'] = linkl  # host halo linking length
    snap.attrs['rhocrit'] = rhocrit  # critical density parameter
    snap.attrs['redshift'] = redshift
    snap.attrs['time'] = t

    # Overwrite the halo IDs array so that at write out it only contains sequential halo IDs of halos above
    # the 10 particle threshold, any halos below this limit are assigned -2 and hence forth considered
    # single particles
    part_haloids = np.full(npart, -2, dtype=int)

    # Sort, derive and assign each of the halo's data to the relevant group within the HDF5 file
    # Loop through halos
    for newID, ID in enumerate(unique_haloids):

        # Extract halo data for this halo ID
        halo_pids = list(assigned_parts[ID])  # Particle ID *** NOTE: Overwrites the C/FORTRAN IDs which start at 1 ***
        halo_poss = pos[halo_pids, :]  # Positions *** NOTE: these are shifted below ***
        halo_vels = vel[halo_pids, :]  # Velocities *** NOTE: these are shifted below ***

        # Reassign the IDs such that they are sequential for halos with 10 or more particles
        part_haloids[halo_pids] = newID

        # Set up arrays for the mean position and mean velocity vectors
        mean_halo_pos = np.zeros(3)
        mean_halo_vel = np.zeros(3)

        # For each dimension wrap each particle if necessary
        for ixyz in [0, 1, 2]:  # loop over dimensions

            # Define the comparison particle as the maximum position in the current dimension
            max_part_pos = halo_poss[:, ixyz].max()

            # Compute all the halo particle separations from the maximum position
            sep = max_part_pos - halo_poss[:, ixyz]

            # If any separations are greater than 50% the boxsize (i.e. the halo is split over the boundary)
            # bring the particles at the lower boundary together with the particles at the upper boundary
            # (ignores halos where constituent particles aren't separated by at least 50% of the boxsize)
            # *** Note: fails if halo's extent is greater than 50% of the boxsize in any dimension ***
            halo_poss[np.where(sep > 0.5 * boxsize), ixyz] += boxsize

            # Compute the shifted mean position in the dimension ixyz
            mean_halo_pos[ixyz] = np.mean(halo_poss[:, ixyz])

            # Centre the halos about the mean in the dimension ixyz
            halo_poss[:, ixyz] -= mean_halo_pos[ixyz]

            # May need to wrap if the halo extends over the upper edge of the box
            mean_halo_pos[ixyz] = mean_halo_pos[ixyz] % boxsize

            # Compute the mean velocity in the dimension ixyz
            mean_halo_vel[ixyz] = np.mean(halo_vels[:, ixyz])

        # Assign the number of particles in the halo
        halo_npart = len(halo_pids)

        # Create datasets in the current halo's group in the HDF5 file
        halo = snap.create_group(str(newID))  # create halo group
        halo.create_dataset('Halo_Part_IDs', shape=[len(halo_pids)], dtype=int, data=halo_pids)  # halo particle ids
        halo.create_dataset('Halo_Pos', shape=halo_poss.shape, dtype=float, data=halo_poss)  # halo centered positions
        halo.create_dataset('Halo_Vel', shape=halo_vels.shape, dtype=float, data=halo_vels)  # halo centered velocities
        halo.create_dataset('mean_pos', shape=mean_halo_pos.shape, dtype=float, data=mean_halo_pos)  # mean position
        halo.create_dataset('mean_vel', shape=mean_halo_vel.shape, dtype=float, data=mean_halo_vel)  # mean velocity
        halo.attrs['halo_nPart'] = halo_npart  # number of particles in halo

        # Run the sub halo finder for each halo in this 'snapshot' and assign the results to the relevant variables
        part_subhaloids, assignedsub_parts = find_subhalos(halo_poss, sub_llcoeff, boxsize, npart)

        # Find the unique subhalo IDs and the number of times they appear
        sub_unique, sub_counts = np.unique(part_subhaloids, return_counts=True)

        # If no subhalos are found continue. If no subhalos are found then only the initialisation
        # values of -2 will be returned
        if sub_unique.size == 1:
            continue

        # Find the subhalos with 10 or more particles by finding the unique IDs in the particle
        # subhalo ids array and finding those IDs that are assigned to 10 or more particles
        subhalos_10plus_inds = np.where(sub_counts >= 10)
        unique_subhaloids = sub_unique[subhalos_10plus_inds]
        sub_counts = sub_counts[subhalos_10plus_inds]

        # Sort sub halos by their mass
        subsinds = sub_counts.argsort()
        unique_subhaloids = unique_subhaloids[subsinds]

        # Assign the number of subhalos found by the subhalo finder in >10, >100, >1000, >10000 criteria
        plus10subs += unique_subhaloids.size
        plus20subs += sub_unique[np.where(sub_counts >= 20)].size
        plus100subs += sub_unique[np.where(sub_counts >= 100)].size
        plus1000subs += sub_unique[np.where(sub_counts >= 1000)].size
        plus10000subs += sub_unique[np.where(sub_counts >= 10000)].size

        # Overwrite the subhalo IDs array so that at write out it only contains sequential halo IDs of subhalos above
        # the 10 particle threshold, any halos below this limit are assigned -2 and hence forth considered
        # single particles
        part_subhaloids = np.full(halo_poss.size, -2, dtype=int)

        # Sort, derive and assign subhalo data to the relevant group within the HDF5 file
        for newsubID, subID in enumerate(unique_subhaloids):  # Loop over the subhalo IDs

            # Extract specific halo data for halo ID
            inds = list(assignedsub_parts[subID])
            subhalo_pids = pid[inds] - 1  # Particle ID *** NOTE: Overwrites the C/FORTRAN IDs which start at 1 ***
            subhalo_poss = halo_poss[subhalo_pids, :]  # Positions *** NOTE: These are shifted below ***
            subhalo_vels = halo_vels[subhalo_pids, :]  # Velocities *** NOTE: These are shifted below ***

            # Reassign the IDs such that they are sequential for halos with 10 or more particles
            part_subhaloids[subhalo_pids] = newsubID

            # Set up arrays for the mean position and mean velocity vectors
            mean_subhalo_pos = np.zeros(3)
            mean_subhalo_vel = np.zeros(3)

            # For each dimension compute the mean position and centre the sub halo positions about it
            for ixyz in [0, 1, 2]:  # loop over dimensions

                # Compute the shifted mean position in the dimension ixyz
                mean_subhalo_pos[ixyz] = np.mean(subhalo_poss[:, ixyz])

                # Centre the halos about the mean in the dimension ixyz
                subhalo_poss[:, ixyz] -= mean_subhalo_pos[ixyz]

                # Compute the mean velocity in the dimension ixyz
                mean_subhalo_vel[ixyz] = np.mean(subhalo_vels[:, ixyz])

            # Assign the number of particles in the halo
            subhalo_npart = len(subhalo_pids)

            # Create datasets in the current halo's group in the HDF5 file
            subhalo = halo.create_group(str(newsubID))  # create halo group
            subhalo.create_dataset('Subhalo_Part_IDs', shape=[len(subhalo_pids)],
                                   dtype=int, data=subhalo_pids)  # halo particle ids
            subhalo.create_dataset('Subhalo_Pos', shape=subhalo_poss.shape, dtype=float,
                                   data=subhalo_poss)  # halo centered positions
            subhalo.create_dataset('Subhalo_Vel', shape=subhalo_vels.shape, dtype=float,
                                   data=subhalo_vels)  # halo centered velocities
            subhalo.create_dataset('subhalo_mean_pos', shape=mean_subhalo_pos.shape, dtype=float,
                                   data=mean_subhalo_pos)  # mean position
            subhalo.create_dataset('subhalo_mean_vel', shape=mean_subhalo_vel.shape, dtype=float,
                                   data=mean_subhalo_vel)  # mean velocity
            subhalo.attrs['subhalo_nPart'] = subhalo_npart  # number of particles in halo

        # Assign the full halo IDs array to the snapshot group
        halo.create_dataset('Subhalo_IDs', shape=part_subhaloids.shape, dtype=int, data=part_subhaloids)

    # Assign the full halo IDs array to the snapshot group
    snap.create_dataset('Halo_IDs', shape=part_haloids.shape, dtype=int, data=part_haloids)

    snap.close()

    # Print out the total number of subhalos in each mass bin
    print(plus10subs, 'subhalos found with 10 or more particles')
    print(plus20subs, 'subhalos found with 20 or more particles')
    print(plus100subs, 'subhalos found with 100 or more particles')
    print(plus1000subs, 'subhalos found with 1000 or more particles')
    print(plus10000subs, 'subhalos found with 10000 or more particles')

# Convert task ID from apollo jobs to the correct form for the halo finder
snapID = int(sys.argv[1])
if snapID < 10:
    snap = '00' + str(snapID)
else:
    snap = '0' + str(snapID)

start = time.time()
hosthalofinder(snap, batchsize=1000000)
print('Total: ', time.time()-start, snap)
