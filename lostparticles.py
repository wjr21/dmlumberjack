import numpy as np
import matplotlib.pyplot as plt


def lostParticleFraction(tree_data, snap, prog_snap, desc_snap, halo):
    """ A function that computes the fraction of the original mass of a halo or its progenitors lost between
    snapshots.

    :param tree_data: The tree data dictionary produced by the Merger Graph.
    :param snap: The snapshot ID.
    :param prog_snap: The progenitor snapshot ID.
    :param desc_snap: The descendant snapshot ID.
    :param halo: The present halo's ID.

    :return: deltaN_prog: The lost particle fraction between the progenitor halos and the present halo.
             deltaN_desc: The lost particle fraction between the present halo and the descendant halos.
    """

    # Extract the progenitor and descendant IDs from the merger graph data.
    prog_ids = tree_data[snap][halo]['Prog_haloIDs'].tolist()
    desc_ids = tree_data[snap][halo]['Desc_haloIDs'].tolist()

    # Extract the particle IDs contained within the present halo
    current_halo_pids = set(tree_data[snap][halo]['current_halo_partIDs'])

    # Extract all particle IDs contained within all progenitors.
    prog_halo_pids = {}  # initialise dictionary for particle IDs in each progenitor
    # If progenitors are present loop over the progenitor IDs and find the lost particle fraction for the
    # progenitor to present halo step
    if len(prog_ids) > 0:
        for prog_id in prog_ids:

            # Assign particle IDs to the dictionary
            prog_halo_pids[prog_id] = tree_data[prog_snap][str(prog_id)]['current_halo_partIDs']

        # Convert progenitor halo IDs dictionary to a single set to utilise set functions later
        # *** NOTE: Using a dictionary and concatenating is more efficient than appending to a large
        # list ***
        prog_halo_pids_set = set(np.concatenate(list(prog_halo_pids.values())))

        # Find the particle IDs that are lost between snapshots
        lost_parts_prog_list = list(current_halo_pids.intersection(prog_halo_pids_set))

        # Compute the lost particle fraction
        deltaN_prog = (float(len(prog_halo_pids_set)) - float(len(lost_parts_prog_list))) / float(len(prog_halo_pids_set))

        # Make sure there are no progenitors where no particles are in the current halo
        assert deltaN_prog != 1.0, 'Progenitor found with no particles in common with it\'s descendant'

    # If no progenitors are found assign a NULL value to the lost particle fraction
    else:
        deltaN_prog = -1.

    # Extract all particle IDs contained within all progenitors.
    desc_halo_pids = {}  # initialise dictionary for particle IDs in each progenitor
    # If descendants are present loop over the descendant IDs and find the lost particle fraction for the
    # present halo to descendant step
    if len(desc_ids) > 0:

        # Handle the -1 place holder for the final snapshot where there are no descendant halos
        if desc_ids != [-1]:


            for desc_id in desc_ids:

                # Assign particle IDs to the dictionary
                desc_halo_pids[desc_id] = tree_data[desc_snap][str(desc_id)]['current_halo_partIDs']

            # Convert descendant halo IDs dictionary to a single set to utilise set functions later
            # *** NOTE: Using a dictionary and concatenating is more efficient than appending to a large
            # list ***
            desc_halo_pids_set = set(np.concatenate(list(desc_halo_pids.values())))

            # Find the particle IDs that are lost between snapshots
            lost_parts_desc_list = list(current_halo_pids.intersection(desc_halo_pids_set))

            # Compute the lost particle fraction
            deltaN_desc = (float(len(current_halo_pids)) - float(len(lost_parts_desc_list))) \
                          / float(len(current_halo_pids))

        # Set place holder for final snapshot halos with no descendants
        else:
          deltaN_desc = -1.

    # If no descendants are found assign a NULL value to the lost particle fraction
    else: 
        deltaN_desc = -1.

    return deltaN_prog, deltaN_desc

def mainLostParticleFraction(tree_data, cutoff=10):
    """ A function which computes the lost particle fraction for every halo in the Merger Graph that
    satisfies the mass threshold and plots a histogram.

    :param tree_data: The tree data dictionary produced by the Merger Graph.
    :param cutoff: The halo mass cutoff in number of particles. Halos under this mass threshold are skipped.

    :return: None
    """

    # Create a snapshot list (past to present day) for looping
    snaplist = []
    for snap in range(0, 62):
        if snap < 10:
            snaplist.append('00' + str(snap))
        elif snap >= 10:
            snaplist.append('0' + str(snap))

    # Initialise the lost particle fraction arrays
    # *** NOTE: These arrays are initialised with considerably more entries than necessary (namely enough
    # entries for every particle to have a logarithmic mass growth), unused entries are removed after all values
    # have been computed.
    deltaN_prog, deltaN_desc = np.zeros(20000000, dtype=float), np.zeros(20000000, dtype=float)

    # Initialise the index counter
    ind_count = -1

    # Loop over the snapshots
    for snap in snaplist:

        # Print the current snapshot to display progress
        print(snap, '%.2f' % (int(snap)/61*100), '%')

        # Define the progenitor and descendant snapshot IDs
        int_snap = int(snap)
        if int_snap > 10:
            prog_snap = '0' +str(int_snap - 1)
        else:
            prog_snap = '00' +str(int_snap - 1)
        if int_snap > 8:
            desc_snap = '0' +str(int_snap + 1)
        else:
            desc_snap = '00' +str(int_snap + 1)

        # Extract the halo IDs as an iterator to loop over
        halo_id_iterator = tree_data[snap].keys()

        # Loop over the halos in this snapshot
        for ind, halo in enumerate(halo_id_iterator):

            # Only perform the computation for halos above the mass threshold
            if tree_data[snap][halo]['current_halo_nPart'] < cutoff:
                continue

            # Increment the index counter
            ind_count += 1

            # Compute the lost particle fractions
            deltaN_prog[ind_count], deltaN_desc[ind_count] = lostParticleFraction(tree_data, snap, prog_snap, desc_snap, halo)

    # Remove the unused entires in the deltaN arrays
    deltaN_prog_final = deltaN_prog[:ind_count+1]
    deltaN_desc_final = deltaN_desc[:ind_count+1]

    # Remove -1s from halos with no progenitors or descendants
    deltaN_prog_final = deltaN_prog_final[np.where(deltaN_prog_final != -1.)]
    deltaN_desc_final = deltaN_desc_final[np.where(deltaN_desc_final != -1.)]

    # Compute particle loss histograms
    H_prog, bin_edges_prog = np.histogram(deltaN_prog_final, bins=100, range=[0.,1.])
    H_desc, bin_edges_desc = np.histogram(deltaN_desc_final, bins=100, range=[0.,1.])

    # Compute the bin widths
    prog_bin_wid = (bin_edges_prog[1] - bin_edges_prog[0])/2.
    desc_bin_wid = (bin_edges_desc[1] - bin_edges_desc[0])/2.

    # =============== Plot the results ===============

    # Set up figure
    fig1 = plt.figure()
    ax4 = fig1.add_subplot(111)

    # Plot the histograms as line plots
    ax4.plot(bin_edges_prog[:-1]+prog_bin_wid, H_prog+1, color='b', label='Progenitor Step')
    ax4.plot(bin_edges_desc[:-1]+desc_bin_wid, H_desc+1, color='g', label='Descendant Step')

    # Set y axis to log scale
    ax4.set_yscale('log')

    # Label axes
    ax4.set_xlabel(r'$\Delta_{N}$')
    ax4.set_ylabel(r'$N+1$')

    # Include legend
    handles, labels = ax4.get_legend_handles_labels()
    ax4.legend(handles, labels)

    # Save the plot as a png
    plt.savefig('Merger Graph Statistics/lostparticles_' + str(cutoff) + '.png', dpi=fig1.dpi)

    # Close the plot to avoid memory use issues when looping over many statistics. (In testing it appears
    # matplotlib doesn't completely clear from memory if lots of plots are made in an outer loop)
    plt.clf()

    return
