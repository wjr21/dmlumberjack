import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import pickle
from treedataloader import loader
from smtloader import opentree
import pprint
import time
import h5py
import os


def directprogdeschist(tree_data):
    """ A function that extracts the number of progenitors and descendants for all halos and
    produces two histograms, one for the number of progenitors and one for the number of
    descendants.

    :param tree_data: The tree data dictionary produced by the Merger Graph.
    :param SMTtreepath: The file path to the SMT algorithms' txt files.

    :return: None
    """

    # Initialise the arrays to store the number of progenitors and descendants
    # *** NOTE: These arrays are initialised with considerably more entries than necessary (namely enough
    # entries for every particle to have a logarithmic mass growth), unused entries are removed after all values
    # have been computed.
    nprogs = np.zeros(20000000, dtype=int)
    ndescs = np.zeros(20000000, dtype=int)

    # Create a snapshot list (past to present day) for looping
    snaplist = []
    for snap in range(0, 62):
        if snap < 10:
            snaplist.append('00' + str(snap))
        elif snap >= 10:
            snaplist.append('0' + str(snap))

    # Initialise array index counter
    ind_count = -1

    # Loop through Merger Graph data assigning each value to the relevant list
    for snap in snaplist:

        # Print the snapshot for progress tracking
        print(snap)

        # Loop over halos within snapshot
        for halo in tree_data[snap].keys():

            # Increment index counter
            ind_count += 1

            # Assign the number of progenitors and descendants
            nprogs[ind_count] = tree_data[snap][halo]['nProg']
            ndescs[ind_count] = tree_data[snap][halo]['nDesc']

    # Remove unused entries
    nprogs = nprogs[:ind_count+1]
    ndescs = ndescs[:ind_count+1]

    # Histogram the results with the number of bins defined such that every number between 0 and
    # the max number of progenitors and descendants has a bin.
    prog_bins = int(np.max(nprogs))
    desc_bins = int(np.max(ndescs))
    HprogDMLJ, binedgesprogDMLJ = np.histogram(nprogs, bins=prog_bins)
    HdescDMLJ, binedgesdescDMLJ = np.histogram(ndescs, bins=desc_bins)

    # =============== Plot the results ===============

    # Set up figure and axes
    fig = plt.figure()
    gs = gridspec.GridSpec(2,2)
    gs.update(wspace=0.5, hspace=0.3)
    ax1 = fig.add_subplot(gs[0,:])
    ax2 = fig.add_subplot(gs[1,:])

    # Plot the histograms
    ax1.bar(binedgesprogDMLJ[:-1]+0.5, HprogDMLJ, color='r', width=1, label='DMLumberJack')
    ax2.bar(binedgesdescDMLJ[:-1]+0.5, HdescDMLJ, color='r', width=1, label='DMLumberJack')

    # # Find, histogram and plot the histograms for the SMT softwares
    # # Find the SMT files for each software
    # treefiles = []
    # for root, dirs, files in os.walk(SMTtreepath):
    #     for name in files:
    #         treefiles.append(os.path.join(root, name))
    #
    # # Compute main branch length and histogram for each SMT software
    # for treefile, color in zip(treefiles, plt.rcParams['axes.prop_cycle'].by_key()['color'][:len(list(treefiles))]):
    #
    #     # Get the snapshot progenitor information
    #     try:
    #         software, tot_nprog, prog_num, halo_progs = opentree(treefile)
    #     except UnicodeDecodeError:
    #         continue
    #     print(software)
    #     # Loop through the halo IDs collecting all progenitor numbers
    #     nprogs = np.zeros(len(prog_num.keys()))  # initialise nprog array
    #     for ind, halo in enumerate(prog_num.keys()):
    #
    #         nprogs[ind] = prog_num[halo]
    #
    #     # Histogram the results
    #     prog_bins = int(np.max(nprogs))
    #     Hprog, binedgesprog = np.histogram(nprogs, bins=prog_bins)
    #     ax1.plot(binedgesprog[:-1]+0.5, Hprog, color=color, linestyle='-', label=software)

    # Set y-axis scaling to logarithmic
    ax1.set_yscale('log')
    ax2.set_yscale('log')

    # Label axes
    ax1.set_xlabel(r'$N_{Prog}$')
    ax2.set_xlabel(r'$N_{Desc}$')
    ax1.set_ylabel(r'$N$')
    ax2.set_ylabel(r'$N$')

    # Include legend
    handles, labels = ax1.get_legend_handles_labels()
    ax1.legend(handles, labels)
    handles, labels = ax2.get_legend_handles_labels()
    ax2.legend(handles, labels)

    # Save the plot as a png
    plt.savefig('Merger Graph Statistics/ProgDescNumberHist.png', dpi=fig.dpi)

    return
