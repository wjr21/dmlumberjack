\contentsline {section}{\numberline {1}Halo finder}{2}
\contentsline {subsection}{\numberline {1.1}DMLumberJack.readgadetdata.readsnapshot}{2}
\contentsline {subsection}{\numberline {1.2}DMLumberJack.kdhalofinder.read\_sim}{3}
\contentsline {subsection}{\numberline {1.3}DMLumberJack.kdhalofinder.find\_halos}{5}
\contentsline {subsection}{\numberline {1.4}DMLumberJack.kdhalofinder.halo\_prit\_stick}{8}
\contentsline {subsection}{\numberline {1.5}DMLumberJack.kdhalofinder.find\_subhalos}{9}
\contentsline {subsection}{\numberline {1.6}DMLumberJack.kdhalofinder.hosthalofinder}{10}
\contentsline {section}{\numberline {2}Merger Graph}{12}
\contentsline {subsection}{\numberline {2.1}DMLumberJack.mergergraph.directProgDescWriter}{12}
\contentsline {subsection}{\numberline {2.2}DMLumberJack.mergergraph.directProgDescFinder}{14}
\contentsline {section}{\numberline {3}Helper Functions}{16}
\contentsline {subsection}{\numberline {3.1}DMLumberJack.SMTloader.opentree}{16}
\contentsline {subsection}{\numberline {3.2}DMLumberJack.SMTloader.AHFhaloreader}{17}
\contentsline {subsection}{\numberline {3.3}DMLumberJack.treedataloader}{18}
\contentsline {section}{\numberline {4}Merger Graph Statistics}{19}
\contentsline {subsection}{\numberline {4.1}DMLumberJack.forestgrowth.forestGrowth}{19}
\contentsline {subsection}{\numberline {4.2}DMLumberJack.forestgrowth.forestGrowthPlot}{20}
\contentsline {subsection}{\numberline {4.3}DMLumberJack.forestgrowth.mainbranchmassSMT}{21}
\contentsline {subsection}{\numberline {4.4}DMLumberJack.forestgrowth.graphplot}{22}
\contentsline {subsection}{\numberline {4.5}DMLumberJack.logMgrowth.logMassGrowth}{23}
\contentsline {subsection}{\numberline {4.6}DMLumberJack.logMgrowth.mainBranchWalkLogM}{24}
\contentsline {subsection}{\numberline {4.7}DMLumberJack.lostparticles.lostParticleFraction}{25}
\contentsline {subsection}{\numberline {4.8}DMLumberJack.lostparticles.lostParticleFractionPlot}{26}
\contentsline {subsection}{\numberline {4.9}DMLumberJack.massfluctuation.massflucuation}{27}
\contentsline {subsection}{\numberline {4.10}DMLumberJack.massfluctuation.mainmfluc}{28}
\contentsline {subsection}{\numberline {4.11}DMLumberJack.mainBranchLength.mainbranchlengthSMT}{29}
\contentsline {subsection}{\numberline {4.12}DMLumberJack.mainBranchLength.mainbranchlengthDMLJ}{30}
\contentsline {subsection}{\numberline {4.13}DMLumberJack.mainBranchLength.mainBranchLengthCompPlot}{31}
\contentsline {subsection}{\numberline {4.14}DMLumberJack.analysis.ProgDescHistogram}{32}
\contentsline {section}{\numberline {5}Data Format}{33}
\contentsline {subsection}{\numberline {5.1}Halo data}{33}
\contentsline {subsection}{\numberline {5.2}Merger Graph data}{36}
\contentsline {subsection}{\numberline {5.3}Halo/Tree Dictionaries}{38}
