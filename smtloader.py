import pickle
import numpy as np

def opentree(treefile):
    """ Takes a SMT merger tree txt file and parses it, returning the contained data.

    :param treefile: The path to the merger tree data file in the form of a string.

    :return: software_info: The software information string.
             tot_nprog: The total number of progenitors found by this piece of software.
             halonprogs: A dictionary containing the number of progenitors of each halo.
             haloprogids: A dictionary containing the IDs of a halo's direct progenitorint.
    """

    # Set up dictionaries to store direct progenitor tree data
    haloprogids = {}
    halonprogs = {}

    # Initialise the current halo counter
    current_halo = 0

    # Initialise the line counter
    count = -1

    # Open the ASCII file and cycle over the lines in the txt file
    for line in open(treefile):

        # Increment the line counter
        count += 1

        # Store the software title (should always be the first word of the first line of the txt file).
        # If it id not then the txt files need editing such that this true, this was the convention 'recommended'
        # for the output of data
        if count == 1:
            words = line.split(' ')
            software = words[0]

        # Store the total number of progenitors found by the software (by convention always the second line)
        if count == 2:
            tot_nprog = line

        # All following lines are tree data or 'END' signifying the end of the data
        elif count > 2:

            # Extract all words in the line
            words = [val for val in line.split() if val.isdigit()]

            # If there is more than 1 word then this is a new halo and all previous progenitor of the last
            # current halo have been assigned
            if len(words) > 1:

                # Assign the number of progenitors for this halo
                halonprogs[words[0]] = int(words[1])

                # Assign the current halo
                current_halo = words[0]

                # Initialise the progenitor IDs entry
                haloprogids[current_halo] = []

            # If there is one word then this is a progenitor of the current halo
            elif len(words) == 1:

                # Assign the progenitor to the current halo entry if it is not the end of the file
                if words != 'END':
                    haloprogids[current_halo].append(''.join(words).rstrip())

    return software, tot_nprog, halonprogs, haloprogids


def AHFhaloreader(halopath):
    """ A function that takes halo finder data in the form of a txt file and write out pickle files
    containing dicitonaries populated with the halo finder data.

    :param halopath: The file path to the halo finder txt file.

    :return: None
    """

    # Create list of files to read and write out
    snapfiles = []
    for root, dirs, files in os.walk(halopath):
        for file in files:
            snapfiles.append(file)
        root = root

    # Create a list of snapshots for looping
    snapshots = []
    for snap in range(0,62):
        if snap < 10:
            snapshots.append('00' + str(snap))
        else:
            snapshots.append('0' + str(snap))

    # Initialise dictionaries for the halo finder data
    haloIDs = {}
    haloNpart = {}
    poss = {}

    # Loop through files loading the data with numpy.loadtxt and assigning values to dictionaries
    for snapkey, snapfile in zip(snapshots,snapfiles):

        # Extract the data
        data = np.loadtxt(root + snapfile, usecols=[0, 4, 5, 6, 7], dtype=str)

        # If the file contains data assign it to the corresponding dictionary
        if data.size > 0:
            haloIDs[snapkey] = data[:, 0]  # halo IDs
            nparts = data[:,1]  # mass
            poss[snapkey] = np.array(data[:, 2:], dtype=float)/1000  # mean halo positions

            # For each halo assign it's mass to the dicitonary
            for ID, npart in zip(list(haloIDs[snapkey]), nparts):
                haloNpart[ID] = float(npart)

        # If the file is empty apart from the header assign None entry
        else:
            haloIDs[snapkey] = None

    # Pickle the dictionaries
    with open('HaloIDs.pck', 'wb') as pfile1:
        pickle.dump(haloIDs, pfile1)
    with open('haloNparts.pck', 'wb') as pfile2:
        pickle.dump(haloNpart, pfile2)
    with open('halopos.pck', 'wb') as pfile3:
        pickle.dump(poss, pfile3)

    return