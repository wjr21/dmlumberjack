import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from collections import defaultdict
import pickle
import copy
import time
import os
from smtloader import *
from kdhalofinder import read_sim
from pygadgetreader import readsnap
from scipy.spatial import cKDTree


def forestGrowth(tree_data, z0halo):
    """ A funciton which traverses a tree including all halos which have interacted with the tree
    in a 'forest/mangrove'.
    
    :param tree_data: The tree data dictionary produced by the Merger Graph.
    :param z0halo: The halo ID of a z=0 halo for which the forest/mangrove plot is desired.

    :return: forest_dict: The dictionary containing the forest. Each key is the snapshot ID and
             the value is a list of halos in this snapshot of the forest.
             massgrowth: The mass history of the forest.
             tree: The dictionary containing the tree. Each key is the snapshot ID and
             the value is a list of halos in this snapshot of the tree.
             main_growth: The mass history of the main branch.
    """

    # Initialise dictionary instances
    forest_dict = defaultdict(set)

    # Create snapshot list in reverse order (present day to past) for the progenitor searching loop
    snaplist = []
    for snap in range(61, 0, -1):
        if snap < 10:
            snaplist.append('00' + str(snap))
        elif snap >= 10:
            snaplist.append('0' + str(snap))

    # Initialise the halo's set for tree walking
    halos = {int(z0halo) + (61 * 100000)}

    # Initialise the forest dictionary with the present day halo as the first entry
    forest_dict['061'] = halos

    # Initialise the set of new found halos used to loop until no new halos are found
    new_halos = halos

    # Initialise the set of found halos used to check if the halo has been found or not
    found_halos = set()

    # Loop until no new halos are found
    count = 0  # initialise the loop counter
    while len(new_halos) != 0:

        # Overwrite the last set of new_halos
        new_halos = set()

        # =============== Progenitors ===============

        # Loop over snapshots and progenitor snapshots
        for prog_snap, snap in zip(snaplist[1:], snaplist[:-1]):

            # Loop over halos in this snapshot
            for halo in halos:

                # Remove snapshot ID from halo ID
                halo -= (int(snap) * 100000)

                # Assign progenitors adding the snapshot * 100000 to the ID to keep track of the snapshot ID
                # in addition to the halo ID
                forest_dict.setdefault(prog_snap, set()).update(set((int(prog_snap) * 100000) +
                                                                     tree_data[snap][str(halo)]['Prog_haloIDs']))

            # Assign the halos variable for the next stage of the tree
            halos = forest_dict[prog_snap]

            # Add any new halos not found in found halos to the new halos set
            new_halos.update(halos - found_halos)

        # If this is the first loop in the while loop assign the merger tree to a variable
        if count == 0:

            # Deepcopy used such that nested entries are not altered after this assignment
            tree = copy.deepcopy(forest_dict)

            # Increment the count to avoid overwriting the tree
            count += 1

        # =============== Descendants ===============

        # Loop over halos found during the progenitor step
        snapshots = list(reversed(list(forest_dict.keys())))
        for desc_snap, snap in zip(snapshots[1:], snapshots[:-1]):

            # Loop over the progenitor halos
            for halo in halos:

                # Remove snapshot ID from halo ID
                halo -= (int(snap) * 100000)

                # Load descendants adding the snapshot * 100000 to keep track of the snapshot ID
                # in addition to the halo ID
                forest_dict.setdefault(desc_snap, set()).update(set((int(desc_snap) * 100000) +
                                                                  tree_data[snap][str(halo)]['Desc_haloIDs']))

            # Assign the halos variable for the next stage of the tree
            halos = forest_dict[desc_snap]

            # Redefine the new halos set to have any new halos not found in found halos
            new_halos.update(halos - found_halos)

        # Add the new_halos to the found halos set
        found_halos.update(new_halos)

    # =============== Forest Mass Growth ===============

    # Initialise mass growth array
    massgrowth = np.zeros(61, dtype=int)

    # Loop over snapshots
    for ind, snap in enumerate(forest_dict.keys()):

        # Get the halo IDs for the current snap
        haloids = forest_dict[snap]

        # Initialise the forest mass
        forestmass = 0

        # Loop over the halos
        for halo in haloids:

            # Remove snapshot ID from halo ID
            halo -= (int(snap) * 100000)

            # Load the halos mass and add it to the snapshot mass
            forestmass += tree_data[snap][str(halo)]['current_halo_nPart']

        # Store total forest mass in the mass dictionary
        massgrowth[ind] = forestmass

    # =============== Merger Tree Mass Growth ===============

    # Initialise mass growth array
    main_growth = np.zeros(61, dtype=int)

    # Loop over snapshots
    for ind, snap in enumerate(tree.keys()):

        # Get the halo IDs for the current snap
        haloids = tree[snap]

        if len(haloids) == 0: continue

        # initialise mass list
        masses = []
        # Loop through halos
        for halo in haloids:

            # Remove snapshot ID from halo ID
            halo -= (int(snap) * 100000)

            # Append main branch mass to the mass list
            masses.append(tree_data[snap][str(halo)]['current_halo_nPart'])

        # Assign maximum halo mass (main branch halo mass) to the main branch mass growth array
        main_growth[ind] = max(masses)

    return forest_dict, massgrowth, tree, main_growth


def forestGrowthPlot(tree_data, z0halo, cutoff):
    """ A function which produces a combined plot of mass growth and a forest plot as well 
    as a plot of mass growth for the forest/mangrove, the main branch and other algorithms 
    with data in the othermergertree directory.
    
    :param tree_data: The tree data dictionary produced by the Merger Graph.
    :param z0halo: The halo ID of a z=0 halo for which the forest/mangrove plot is desired.
    :param cutoff: The mass (number of particle) threshold which halos must be above.
    
    :return: None
    """

    # Build the forest
    forest_dict, massgrowth, tree, main_growth = forestGrowth(tree_data, z0halo)

    # Initialise the number of particles per halo dictionary
    forest_nPart = {}

    # Loop over snapshots
    for snap in forest_dict.keys():

        # Initialise dictionary entry
        forest_nPart[snap] = []

        # Loop through halos in this snapshot adding their mass
        for halo in forest_dict[snap]:

            # Remove snapshot ID from halo ID
            halo -= (int(snap) * 100000)

            mass = tree_data[snap][str(halo)]['current_halo_nPart']
            forest_nPart[snap].append(mass)

    # =============== Match To SMT ===============

    # Assign this halo's particle IDs
    this_halo_part = tree_data['061'][str(z0halo)]['current_halo_partIDs']

    # Load positional data from this snapshot
    pid, pos, vel, npart, boxsize, redshift, t, rhocrit, pmass, h, linkl = read_sim('061', 'snapshotdata/snapdir_')
    sinds = pid.argsort()
    pos = pos[sinds, :]

    # Extract this halos positions (pre wrapping)
    halo_query_pos = pos[this_halo_part]

    # Load AHF positions and IDs
    with open('HaloIDs.pck', 'rb') as pfile1:
        AHFIDs = pickle.load(pfile1)['061']
    with open('halopos.pck', 'rb') as pfile2:
        AHF_poss = pickle.load(pfile2)['061']

    # Build KDtree from AHF halo positions
    tree = cKDTree(AHF_poss, leafsize=16, compact_nodes=True, balanced_tree=True, boxsize=[boxsize, boxsize, boxsize])

    # Query the tree with the halo positions
    dists, query_inds = tree.query(halo_query_pos, k=1, n_jobs=-1)

    # Find the unique query_inds
    unique, counts = np.unique(query_inds, return_counts=True)

    # In the case multiple halo ids are returned only take the first for comparison (highest counts=highest number
    # of particles in common)
    AHFhalo = AHFIDs[unique[0]]

    # Find the SMT filepaths for each algorithm
    treefiles = []
    for root, dirs, files in os.walk('Othermergertreedata/SussingMergerTrees'):
       for name in files:
          treefiles.append(os.path.join(root, name))

    # Loop through filepaths
    SMT_growths = {}
    for treefile in iter(treefiles):

        # Get the snapshot progenitor information
        try:
            software, tot_nprog, prog_num, halo_progs = opentree(treefile)
        except UnicodeDecodeError:
            continue

        # Compute and assign the mas history for this algorithm
        SMT_growths[software] = mainbranchmassSMT(AHFhalo, prog_num, halo_progs)

    # =============== Plot the results ===============

    # Set up plot and axes
    fig1 = plt.figure(figsize=(8,6))
    gs = gridspec.GridSpec(3,2)
    gs.update(wspace=0.5, hspace=0.0)
    ax1 = fig1.add_subplot(gs[0:2,:])
    ax2 = fig1.add_subplot(gs[2,:])

    # Plot grid lines
    ax1.grid(b=True, which='major', color='#545556', linestyle='-', alpha=0.3)
    ax2.grid(b=True, which='major', color='#545556', linestyle='-', alpha=0.3)

    # Plot full forest
    forest_xs = []
    forest_ys = []
    masses = []
    for snap in forest_nPart.keys():
        y_range = list(range(0, len(forest_nPart[snap])))
        forest_ys.extend(y_range)  # xs is a range from 0 to the number of halos in each snapshot
        forest_xs.extend(np.full_like(y_range, int(snap)))  # ys is then simply an array of the snapshot ID

        # Assign masses to the mass list
        masses.extend(np.sort(np.array(forest_nPart[snap], copy=False, dtype=float))[::-1])

    # Convert masses to an array
    masses = np.array(masses, copy=False)

    # Compute the markersize from the mass
    markersize = masses * 1. / masses.max() * 30

    # Plot the forest
    ax1.scatter(forest_xs[::-1], forest_ys[::-1], c=masses[::-1], marker='o', s=markersize[::-1], cmap='jet')

    # Plot the mass growth
    # Get the xs and ys for the plot from the snapshot IDs and masses
    xs = [int(snap) for snap in forest_dict.keys()]
    ys = massgrowth
    xs = np.array(xs, copy=False)

    # Sort results by the snapshot ID
    sorting_inds = xs.argsort()
    xs = xs[sorting_inds]
    ys = ys[sorting_inds]
    ax2.plot(xs, ys, color='r')

    # Label axis
    ax1.set_ylabel(r'$N_{halo}$')
    ax2.set_xlabel(r'$S$')
    ax2.set_ylabel(r'$M_{forests}$')

    # Save as a png
    plt.savefig('Forests/>' + str(cutoff) + '/forest_growth' + str(z0halo) + '.png', dpi=600)

    # Close the plot so not interfere with the next plot
    plt.close()

    # Set up figure and axes
    fig2 = plt.figure(figsize=(8,6))
    gs = gridspec.GridSpec(3,3)
    gs.update(wspace=0.5, hspace=0.0)
    ax1 = fig2.add_subplot(gs[0,:2])
    ax2 = fig2.add_subplot(gs[1,:2])
    ax3 = fig2.add_subplot(gs[2,:2])
    ax4 = fig2.add_subplot(gs[1, 2])

    # Plot grid lines
    ax1.grid(b=True, which='major', color='#545556', linestyle='-', alpha=0.3)
    ax2.grid(b=True, which='major', color='#545556', linestyle='-', alpha=0.3)
    ax3.grid(b=True, which='major', color='#545556', linestyle='-', alpha=0.3)

    # Plot data
    ax1.plot(xs, ys, color='r', label='Forest Mass Growth')
    ax2.plot(xs, main_growth[::-1], color='g', label='Main Branch Mass Growth')

    # Plot ax1 and ax2 data on axis 3 with 0 length for convenient labelling in the legend
    ax3.plot([1.0, 1.0], [1.0, 1.0], color='r', label='DMLJ Forest')
    ax3.plot([1.0, 1.0], [1.0, 1.0], color='g', label='DMLJ Main Branch')

    # Plot the SMT algorithms
    for software, color in zip(SMT_growths.keys(),
                               plt.rcParams['axes.prop_cycle'].by_key()['color'][:len(list(SMT_growths.keys()))]):
        ax3.plot(xs, SMT_growths[software], linestyle='--', color=color, label=software)

    # Set labels
    ax3.set_xlabel(r'$S$')
    ax2.set_ylabel(r'$M$')

    # Set y axis limits such that 0 is removed from the upper two subplots to avoid tick stacking
    ax1.set_ylim(0.5, None)
    ax2.set_ylim(0.5, None)

    # Create legend
    handles, labels = ax3.get_legend_handles_labels()
    ax4.legend(handles, labels, loc='right')

    # Remove axis from ax4
    ax4.set_axis_off()

    # Save as a png
    plt.savefig('Forests/>' + str(cutoff) + '/massgrowths' + str(z0halo) + '.png', dpi=600)
    plt.close()

    return

def mainbranchmassSMT(z0halo, prog_num, halo_progs):
    """ A function to extract the mass history from  SMT algorithm data.
    
    :param z0halo: The halo ID of a z=0 halo.
    :param prog_num: The array of progenitor numbers of all halos in the SMT algorithm.
    :param halo_progs: The array of progenitor IDs of all halos in the SMT algorithm.

    :return: mainbranch_mass: The array containing the mass history for this algorithm.
    """

    # Open the halo mass (number of particles) data
    with open('haloNparts.pck', 'rb') as pfile2:
        halo_Npart = pickle.load(pfile2)

    # Initialise index counter
    ind = -1

    # Assign z=0 halo ID to halo pointer for walking
    halo = z0halo

    # Initialise mass history array
    mainbranch_mass = np.zeros(61, dtype=int)

    # Until a halo with no progenitors is found, loop
    while prog_num[halo] != 0:

        # Increment the index counter
        ind += 1

        # Initialise the progenitor mass list
        prog_nparts = []

        # Loop over progenitors appending their mass
        for prog in halo_progs[halo]:

            # Some algorithms introduce halo IDs not contained in AHF so these must be ignored
            # ending the walk
            try:
                prog_nparts.append(halo_Npart[prog])
            except KeyError:
                continue

        # Convert mass list to an array
        prog_nparts_arr = np.array(prog_nparts, copy=False, dtype=int)

        # Deal with the case where no progenitors are found at this point
        if prog_nparts_arr.size == 0:

            return mainbranch_mass[::-1]

        # If there is only one progenitor assign it as the next step along the main branch
        elif prog_nparts_arr.size == 1:
            main = halo_progs[halo][0]

        # Else assign the progenitor with the highest mass as the next step along the main branch
        else:
            main = str(halo_progs[halo][prog_nparts_arr.argmax()])
        halo = main

        # Assign main branch mass to the mass array (this is revered upon return such that it goes from
        # beginning of time to the present day)
        mainbranch_mass[ind] = prog_nparts_arr.max()

    return mainbranch_mass[::-1]

def graphplot(tree_data, z0halo):
    """ A function that produces snapshot vs mean x graphics to visualise forests/mangroves.
    
    :param tree_data: The tree data dictionary produced by the Merger Graph.
    :param z0halo: The halo ID of a z=0 halo for which the forest/mangrove plot is desired.
    
    :return: None
    """

    # Run the forest growth function to get the forest and mass histories
    forest_dict, massgrowth, tree, main_growth = forestGrowth(tree_data, z0halo)

    # Load positional data from the simulation data
    poss = {}
    for snap in forest_dict.keys():
        
        # Print progress
        print('Loading: ', snap)

        # Load positional data
        pos = readsnap('snapshotdata/snapdir_' + snap + '/62.5_dm_' + snap, 'pos', 1)

        # Load particle IDs
        pid = readsnap('snapshotdata/snapdir_' + snap + '/62.5_dm_' + snap, 'pid', 1)

        # Sort by particle ID and assign to dictionary entry
        sinds = pid.argsort()
        poss[snap] = pos[sinds, :]

    # Extract spatial data for the forest
    xs = []
    snaps = []
    masses = []
    main_snaps = []
    main_xs = []
    main_masses = []
    main_IDs = []
    main_xs_dict = {}
    for snap in forest_dict.keys():  # loop through snapshots
        
        # Print progress
        print('Forest', snap)

        # Initialise the arrays to store this snapshot's forest data
        this_snap_xs = np.zeros(len(forest_dict[snap]))
        this_snap_masses = np.zeros(len(forest_dict[snap]))
        this_snap_IDs = np.zeros(len(forest_dict[snap]))
        this_snap_snaps = np.zeros(len(forest_dict[snap]))

        # Loop through halos
        for ind, halo in enumerate(forest_dict[snap]):

            # Assign this halo to the snapshot IDs array
            this_snap_IDs[ind] = halo

            # Remove snapshot ID from the halo ID
            halo -= (int(snap) * 100000)

            # Extract the halo's x position and assign it to the snapshot's position array
            halo_mean_pos = poss[snap][tree_data[snap][str(halo)]['current_halo_partIDs'], :]
            x = np.mean(halo_mean_pos[:,0])
            this_snap_xs[ind] = x

            # Assign the halo mass to this snapshot's mass array
            this_halo_mass = tree_data[snap][str(halo)]['current_halo_nPart']
            this_snap_masses[ind] = this_halo_mass

            # Assign the snapshot for this halo halo to it's array
            this_snap_snaps[ind] = int(snap)

        # Centre this snapshot on the main branch and assign this snapshot's forest data
        # to the lists for plotting
        if len(this_snap_masses) > 0:
            mainx = this_snap_xs[this_snap_masses.argmax()]
            cent_rs = this_snap_xs - mainx
            xs.extend(cent_rs)
            snaps.extend(this_snap_snaps)
            masses.extend(this_snap_masses)

            # Assign main branch data to the main branch lists for plotting
            main_IDs.append(this_snap_IDs[this_snap_masses.argmax()])
            main_snaps.append(int(snap))
            main_masses.append(this_snap_masses.max())
            main_xs.append(0)
            main_xs_dict[snap] = mainx

    # Extract tree data to compare the forest to the tree
    tree_xs = []
    tree_snaps = []
    tree_masses = []
    for snap in tree.keys():

        # Print progress
        print('Tree', snap)

        # Initialise arrays for the tree data
        this_snap_tree_xs = np.zeros(len(tree[snap]))
        this_snap_tree_masses = np.zeros(len(tree[snap]))
        this_snap_tree_snaps = np.zeros(len(tree[snap]))

        # Loop through the halos in this tree at this snapshot
        for ind, halo in enumerate(tree[snap]):

            # Remove snapshot ID from halo ID
            halo -= (int(snap) * 100000)

            # Extract the halo's x position and assign it to the snapshot's position array
            halo_mean_pos = poss[snap][tree_data[snap][str(halo)]['current_halo_partIDs'], :]
            x = np.mean(halo_mean_pos[:,0])
            this_snap_tree_xs[ind] = x

            # Assign the halo mass to this snapshot's mass array
            this_halo_tree_mass = tree_data[snap][str(halo)]['current_halo_nPart']
            this_snap_tree_masses[ind] = this_halo_tree_mass

            # Assign the snapshot for this halo halo to it's array
            this_snap_tree_snaps[ind] = int(snap)

        # Centre this snapshot on the main branch and assign this snapshot's forest data
        # to the lists for plotting
        if len(this_snap_tree_masses) > 0:
            mainx = this_snap_tree_xs[this_snap_tree_masses.argmax()]
            cent_tree_xs = this_snap_tree_xs - mainx
            tree_xs.extend(cent_tree_xs)
            tree_snaps.extend(this_snap_tree_snaps)
            tree_masses.extend(this_snap_tree_masses)

    # =============== Create 'Branches' Of The Tree For Plotting ===============

    # Initialise lists to avoid double tracing halos
    done_halos = []
    branchxs = {}
    branch_snaps = {}

    # Loop from beginning of time track descendant branches
    for num, snap in enumerate(reversed(list(forest_dict.keys()))):

        print('Branches: ', snap)

        # Loop through halos
        for halo in forest_dict[snap]:

            # Assign the beginning halo to pointer
            start_halo = halo

            # Ignore halos in the main branch
            if halo not in main_IDs and halo not in done_halos:

                # Remove snapshot ID from halo ID
                halo -= (int(snap) * 100000)

                # Initialise branch
                branchxs[start_halo] = []

                # Extract the halo's x position and assign it to the snapshot's position array
                halo_mean_pos = poss[snap][tree_data[snap][str(halo)]['current_halo_partIDs'], :]
                x = np.mean(halo_mean_pos[:,0])

                # Centre the halo's position on the main branch
                branchxs[start_halo].append(x - main_xs_dict[snap])

                # Assign the snapshot plotting
                branch_snaps[start_halo] = []
                branch_snaps[start_halo].append(int(snap))

                # Assign the snapshot ID to a variable for walking down the tree
                desc_snap = snap

                # Initialise END condition
                END = False

                # Walk down descendants
                while int(desc_snap) < 61:

                    # Finish walking the tree if the end has been reached
                    if END == True: break

                    # Extract the descendants and end the walk if no descendants are found
                    descs = tree_data[desc_snap][str(halo)]['Desc_haloIDs']
                    if len(descs) != 0:
                        desc = descs[0]
                    else:
                        break

                    # Compute the descendant snapshot ID
                    if int(snap) > 8:
                        desc_snap = '0' + str(int(desc_snap) + 1)
                    else:
                        desc_snap = '00' + str(int(desc_snap) + 1)

                    # Extract the next halo in this branch's x position and centre it on the main branch
                    halo_mean_pos = poss[desc_snap][tree_data[desc_snap][str(desc)]['current_halo_partIDs'], :]
                    x = np.mean(halo_mean_pos[:,0])
                    branchxs[start_halo].append(x - main_xs_dict[desc_snap])

                    # Assing the descendant snapshot ID for plotting
                    branch_snaps[start_halo].append(int(desc_snap))
                    done_halos.append((int(desc_snap) * 100000) + desc)

                    # Assign the descendant halo to the halo pointer for the next loop in the walk
                    halo = desc

                # =============== Add In The Branches Progenitor To The Start ================

                # Remove the snapshot ID information from the start halo's ID
                first_halo = start_halo - (int(snap) * 100000)

                # Extract the progenitor ID of the main progenitor
                progs = tree_data[snap][str(first_halo)]['Prog_haloIDs']
                if len(progs) != 0:
                    prog = progs[0]
                else:  # if there is no progenitor simply move to the next halo
                    continue

                # Define the progenitor snapshot ID
                if int(snap) > 10:
                    prog_snap = '0' + str(int(snap) - 1)
                else:
                    prog_snap = '00' + str(int(snap) - 1)

                # Extract this progenitor's x position, centre it and apply it to the beginning of the branch list
                halo_mean_pos = poss[prog_snap][tree_data[prog_snap][str(prog)]['current_halo_partIDs'], :]
                x = np.mean(halo_mean_pos[:,0])
                branchxs[start_halo].insert(0, (x - main_xs_dict[prog_snap]))

                # Assing the porgenitor snapshot ID for plotting
                branch_snaps[start_halo].insert(0, int(prog_snap))

    # Convert masses to an array
    masses = np.array(masses, copy=False, dtype=float)

    # Set up plot
    fig = plt.figure(figsize=(5.8, 9.0))
    ax = fig.add_subplot(111)

    # Extract color cycle from matplotlib for plotting branches
    colors = plt.rcParams['axes.prop_cycle'].by_key()['color'][1:]

    # Plot the branches
    for num, halo in enumerate(branchxs.keys()):

        # Plot the current branch
        ax.plot(branchxs[halo], branch_snaps[halo], linewidth=0.5, color=colors[num%len(colors)], zorder=2)

    # Plot the forest, tree and main branch
    ax.plot(main_xs, main_snaps, linewidth=3, color='#F2C75C', zorder=1)
    ax.scatter(xs, snaps, s=masses/max(masses)*50, zorder=3, color='#874187')
    ax.scatter(tree_xs, tree_snaps, s=tree_masses/max(masses)*50, color='#1E428A', zorder=4)

    # Get axis limits
    ymin, ymax = min(snaps), max(snaps)
    xmin, xmax = ax.get_xlim()

    # Get the width and height of axes to compute arrowhead length and width
    dps = fig.dpi_scale_trans.inverted()
    bbox = ax.get_window_extent().transformed(dps)
    width, height = bbox.width, bbox.height

    # Compute arrowhead width and length
    hw = 1./20.*(ymax-ymin)
    hl = 1./20.*(xmax-xmin)

    # Compute arrowhead length and width
    yhw = hw/(ymax-ymin)*(xmax-xmin)* height/width
    yhl = hl/(xmax-xmin)*(ymax-ymin)* width/height

    # Remove the spines
    for side in ['bottom','right','top','left']:
        ax.spines[side].set_visible(False)

    # Remove ticks and labels
    ax.set_xticks([]) # labels
    ax.set_yticks([])
    ax.xaxis.set_ticks_position('none') # tick markers
    ax.yaxis.set_ticks_position('none')

    # Plot the arrow
    ax.arrow(xmin, ymin, 0., ymax-ymin, fc='#D0D3D4', ec='#D0D3D4', lw=2., head_width=yhw,
             head_length=yhl, overhang =0.3, length_includes_head=True, clip_on =False)

    # Invert y axis
    ax.set_ylim(ax.get_ylim()[::-1])

    # Save as a png
    plt.savefig('treeplot_' + str(z0halo) + '.png', dpi=600, transparent=True)
