import h5py
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.gridspec as gridspec
import numpy as np

def vectorplot(treepath, halopath, halo_ID, snap, length=0.02, output='save'):
    """ A function which takes a halo and plots a vector diagram of it's self, its progenitor halos and
    it's descendant halos. It produces 3 2D plots from each possible orientation and a 3D plot.

    :param treepath: The filepath to the Merger Graph HDF5 file.
    :param halopath: The filepath to the halo finder HDF5 file.
    :param halo_ID: The ID of the halo for velocity mapping.
    :param snap: The snapshot ID of the halo.
    :param length: The length of vectors on the resultant plot.
    :param output: Either 'save' or 'show' depending on the desired ouput for the plot, 'save' saves a png
    and 'show' outputs it to an interactive window.

    :return: None
    """

    # Load graph data from HDF5 file
    current_hdf = h5py.File(treepath + snap + '.hdf5', 'r')

    # Extract current halo progenitor and descendant IDs
    prog_ids = current_hdf[str(halo_ID)]['Prog_haloIDs'].value
    desc_ids = current_hdf[str(halo_ID)]['Desc_haloIDs'].value

    current_hdf.close()

    # Load halo data
    current_hdf = h5py.File(halopath + snap + '.hdf5', 'r')

    # Extract mean position and mean velocity for the current halo
    halo_mean_vel = current_hdf[str(halo_ID)]['mean_vel'].value
    halo_mean_pos = current_hdf[str(halo_ID)]['mean_pos'].value

    # Extract the halos mass
    halo_mass = current_hdf[str(halo_ID)].attrs['halo_nPart']
    
    current_hdf.close()

    # Compute the progenitor snapshot ID
    if int(snap) > 10:
        prog_snap = '0' + str(int(snap) - 1)
    else:
        prog_snap = '00' + str(int(snap) - 1)

    # Load progenitor halo data
    prog_hdf = h5py.File(halopath + prog_snap + '.hdf5', 'r')

    # Initialise the index counter
    ind_count = -1

    # Initialise the progenitor data arrays
    prog_vels = np.zeros([prog_ids.size, 3])
    prog_poss = np.zeros([prog_ids.size, 3])
    prog_masses = np.zeros(prog_ids.size)

    # Extract the mean position, mean velocity and mass for all progenitors
    for ID in prog_ids:
        count += 1  # increment the index counter
        prog_vels[ind_count, :] = prog_hdf[str(ID)]['mean_vel'].value
        prog_poss[ind_count, :] = prog_hdf[str(ID)]['mean_pos'].value
        prog_masses[ind_count] = prog_hdf[str(ID)].attrs['halo_nPart']
        
    prog_hdf.close()

    # Compute the descendant snapshot ID
    if int(snap) > 8:
        desc_snap = '0' + str(int(snap) + 1)
    else:
        desc_snap = '00' + str(int(snap) + 1)

    # Load the descendant halo data
    desc_hdf = h5py.File(halopath + desc_snap + '.hdf5', 'r')
    
    # Initialise the index counter
    ind_count = -1

    # Initialise the descendant data arrays
    desc_vels = np.zeros([desc_ids.size, 3])
    desc_poss = np.zeros([desc_ids.size, 3])
    desc_masses = np.zeros(desc_ids.size)

    # Extract the mean position, mean velocity and mass for all descendants
    for ID in desc_ids:
        count += 1  # increment the index counter
        desc_vels[count, :] = desc_hdf[str(ID)]['mean_vel'].value
        desc_poss[count, :] = desc_hdf[str(ID)]['mean_pos'].value
        desc_masses[count] = desc_hdf[str(ID)].attrs['halo_nPart']
        
    desc_hdf.close()
    
    # Convert masses into marker sizes relative to the current halo's mass
    max_mass = halo_mass
    desc_size = desc_masses / max_mass * 100
    prog_size = prog_masses / max_mass * 100
    halo_size = 100

    # Compute the vector lengths
    halo_length = [np.sqrt(halo_mean_vel[0]**2 + halo_mean_vel[1]**2 + halo_mean_vel[2]**2)]
    prog_lengths = np.sqrt(prog_vels[:, 0]**2 + prog_vels[:, 1]**2 + prog_vels[:, 2]**2)
    desc_lengths = np.sqrt(desc_vels[:, 0]**2 + desc_vels[:, 1]**2 + desc_vels[:, 2]**2)

    # Compute the max velocity
    max_vel = np.concatenate([halo_length, prog_lengths, desc_lengths]).max()

    # Normalise the halo velocities and multiply by the vector length
    prog_vels = prog_vels / max_vel * length
    desc_vels = desc_vels / max_vel * length
    halo_mean_vel = halo_mean_vel / max_vel * length

    # =============== Plot the results ===============

    # Set up the figure and axes
    fig = plt.figure(figsize=(8,8))
    gs = gridspec.GridSpec(3,3)
    gs.update(wspace=0.5, hspace=0.5)
    ax = fig.add_subplot(gs[1:3,:-1], projection='3d')
    axxy = fig.add_subplot(gs[0,0])
    axyz = fig.add_subplot(gs[0,1])
    axxz = fig.add_subplot(gs[0,2])
    leg = fig.add_subplot(gs[1:3,-1])
    
    # plot halos in the 3d plot
    ax.scatter(prog_poss[:, 0], prog_poss[:, 1], prog_poss[:, 2], color='b', s=prog_size, Label='Progenitor', alpha=0.6)
    ax.scatter(desc_poss[:, 0], desc_poss[:, 1], desc_poss[:, 2], color='g', s=desc_size, Label='Descendant', alpha=0.6)
    ax.scatter(halo_mean_pos[0], halo_mean_pos[1], halo_mean_pos[2], color='r', s=halo_size, Label='Current', alpha=0.6)

    # Plot velocity vectors in the 3d plot
    ax.quiver(prog_poss[:, 0], prog_poss[:, 1], prog_poss[:, 2], prog_vels[:, 0], prog_vels[:, 1], prog_vels[:, 2],
              color='b', arrow_length_ratio=0.3, alpha=0.6)
    ax.quiver(desc_poss[:, 0], desc_poss[:, 1], desc_poss[:, 2], desc_vels[:, 0], desc_vels[:, 1], desc_vels[:, 2],
              color='g', arrow_length_ratio=0.3, alpha=0.6)
    ax.quiver(halo_mean_pos[0], halo_mean_pos[1], halo_mean_pos[2], halo_mean_vel[0], halo_mean_vel[1], halo_mean_vel[2],
              color='r', arrow_length_ratio=0.3, alpha=0.6)
    
    # plot halos for xy orientation
    axxy.scatter(prog_poss[:, 0], prog_poss[:, 1], color='b', s=prog_size, Label='Progenitor', alpha=0.6)
    axxy.scatter(desc_poss[:, 0], desc_poss[:, 1], color='g', s=desc_size, Label='Descendant', alpha=0.6)
    axxy.scatter(halo_mean_pos[0], halo_mean_pos[1], color='r', s=halo_size, Label='Current', alpha=0.6)

    # Plot velocity vectors for xy orientation
    axxy.quiver(prog_poss[:, 0], prog_poss[:, 1], prog_vels[:, 0], prog_vels[:, 1],
              color='b', alpha=0.6, units='xy')
    axxy.quiver(desc_poss[:, 0], desc_poss[:, 1], desc_vels[:, 0], desc_vels[:, 1],
              color='g', alpha=0.6, units='xy')
    axxy.quiver(halo_mean_pos[0], halo_mean_pos[1], halo_mean_vel[0], halo_mean_vel[1],
              color='r', alpha=0.6, units='xy')
    
    # plot halos for xz orientation
    axxz.scatter(prog_poss[:, 0], prog_poss[:, 2], color='b', s=prog_size, Label='Progenitor', alpha=0.6)
    axxz.scatter(desc_poss[:, 0], desc_poss[:, 2], color='g', s=desc_size, Label='Descendant', alpha=0.6)
    axxz.scatter(halo_mean_pos[0], halo_mean_pos[2], color='r', s=halo_size, Label='Current', alpha=0.6)

    # Plot velocity vectors for xz orientation
    axxz.quiver(prog_poss[:, 0], prog_poss[:, 2], prog_vels[:, 0], prog_vels[:, 2],
              color='b', alpha=0.6, units='xy')
    axxz.quiver(desc_poss[:, 0], desc_poss[:, 2], desc_vels[:, 0], desc_vels[:, 2],
              color='g', alpha=0.6, units='xy')
    axxz.quiver(halo_mean_pos[0], halo_mean_pos[2], halo_mean_vel[0], halo_mean_vel[2],
              color='r', alpha=0.6, units='xy')
    
    # plot halos for yz orientation
    axyz.scatter(prog_poss[:, 1], prog_poss[:, 2], color='b', s=prog_size, Label='Progenitor', alpha=0.6)
    axyz.scatter(desc_poss[:, 1], desc_poss[:, 2], color='g', s=desc_size, Label='Descendant', alpha=0.6)
    axyz.scatter(halo_mean_pos[1], halo_mean_pos[2], color='r', s=halo_size, Label='Current', alpha=0.6)

    # Plot velocity vectors for yz orientation
    axyz.quiver(prog_poss[:, 1], prog_poss[:, 2], prog_vels[:, 1], prog_vels[:, 2],
              color='b', alpha=0.6, units='xy')
    axyz.quiver(desc_poss[:, 1], desc_poss[:, 2], desc_vels[:, 1], desc_vels[:, 2],
              color='g', alpha=0.6, units='xy')
    axyz.quiver(halo_mean_pos[1], halo_mean_pos[2], halo_mean_vel[1], halo_mean_vel[2],
              color='r', alpha=0.6, units='xy')

    # Label axis
    ax.set_xlabel('x')
    ax.set_ylabel('y')
    ax.set_zlabel('z')
    axxy.set_xlabel('x')
    axxy.set_ylabel('y')
    axyz.set_xlabel('y')
    axyz.set_ylabel('z')
    axxz.set_xlabel('x')
    axxz.set_ylabel('z')

    # Include legend
    handles, labels = axxy.get_legend_handles_labels()
    leg.legend(handles, labels)

    # Remove the sacrificial axis for the legend
    leg.set_axis_off()

    # Save figure or output it to interactive window
    if output == 'save':
        plt.savefig('Velocity plots/VelocityPlot_' + str(halo_ID) + '_3D.png', dpi=800)
    elif output == 'show':
        plt.show()

    return

list = [0, 1, 100, 1000, 1001, 8728, 40000]
lengths = [0.1, 0.1, 0.02, 0.04, 0.04, 0.2, 0.02]
for haloid, length in zip(list, lengths):
    vectorplot('MergerGraphs/Mgraph_', 'halo_snapshots/halos_', haloid, '052', length=length, output='save')
