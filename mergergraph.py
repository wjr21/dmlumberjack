import numpy as np
import h5py
import time
import sys

def directProgDescFinder(current_halo_pids, prog_snap_haloIDs, desc_snap_haloIDs, 
                         prog_counts, desc_counts, part_threshold):
    """

    :param current_halo_pids:
    :param prog_snap_haloIDs:
    :param desc_snap_haloIDs:
    :param prog_counts:
    :param desc_counts:
    :param part_threshold:
    :return:
    """

    # =============== Find Progenitor IDs ===============

    # If any progenitor halos exist (i.e. The current snapshot ID is not 000, enforced in the main function)
    if prog_snap_haloIDs.size != 0:

        # Find the halo IDs of the current halo's particles in the progenitor snapshot by indexing the
        # progenitor snapshot's particle halo IDs array with the halo's particle IDs, this can be done
        # since the particle halo IDs array is sorted by particle ID.
        prog_haloids = prog_snap_haloIDs[current_halo_pids]

        # Find the unique halo IDs and the number of times each appears
        uniprog_haloids, uniprog_counts = np.unique(prog_haloids, return_counts=True)

        # Remove single particle halos (ID=-2), since np.unique returns a sorted array this can be
        # done by removing the first value.
        if -2 in uniprog_haloids:
            uniprog_haloids = uniprog_haloids[1:]
            uniprog_counts = uniprog_counts[1:]
        
        # Remove halos below the Progenitor/Descendant mass threshold (unnecessary with a part_threshold
        # of 10 since the halo finder only returns halos with 10 or more particles)
        if part_threshold > 10:
            uniprog_haloids = uniprog_haloids[np.where(prog_counts[uniprog_haloids] >= part_threshold)]
            uniprog_counts = uniprog_counts[np.where(prog_counts[uniprog_haloids] >= part_threshold)]

        # Find the number of progenitor halos from the size of the unique array
        nprog = uniprog_haloids.size

        # Assign the corresponding number of particles in each progenitor for sorting and storing
        # This can be done simply by using the ID of the progenitor since again np.unique returns
        # sorted results.
        prog_npart = prog_counts[uniprog_haloids]

        # Sort the halo IDs and number of particles in each progenitor halo by their contribution to the
        # current halo (number of particles from the current halo in the progenitor or descendant)
        sorting_inds = uniprog_counts.argsort()[::-1]
        prog_npart = prog_npart[sorting_inds]
        prog_haloids = uniprog_haloids[sorting_inds]
        prog_mass_contribution = uniprog_counts[sorting_inds]

    # If there is no progenitor store Null values
    else:
        nprog = 0
        prog_npart = np.array([-1], copy=False, dtype=int)
        prog_haloids = np.array([-1], copy=False, dtype=int)
        prog_mass_contribution = np.array([-1], copy=False, dtype=int)

    # =============== Find Descendant IDs ===============

    # If descendant halos exist (i.e. The current snapshot ID is not 061, enforced in the main function)
    if desc_snap_haloIDs.size != 0:

        # Find the halo IDs of the current halo's particles in the descendant snapshot by indexing the
        # descendant snapshot's particle halo IDs array with the halo's particle IDs, this can be done
        # since the particle halo IDs array is sorted by particle ID.
        desc_haloids = desc_snap_haloIDs[current_halo_pids]

        # Find the unique halo IDs and the number of times each appears
        unidesc_haloids, unidesc_counts = np.unique(desc_haloids, return_counts=True)

        # Remove single particle halos (ID=-2) for the counts, since np.unique returns a sorted array this can be
        # done by removing the first value.
        if -2 in unidesc_haloids:
            unidesc_haloids = unidesc_haloids[1:]
            unidesc_counts = unidesc_counts[1:]
        
        # Remove halos below the Progenitor/Descendant mass threshold (unnecessary with a part_threshold
        # of 10 since the halo finder only returns halos with 10 or more particles)
        if part_threshold > 10:
            unidesc_haloids = unidesc_haloids[np.where(desc_counts[unidesc_haloids] >= part_threshold)]
            unidesc_counts = unidesc_counts[np.where(desc_counts[unidesc_haloids] >= part_threshold)]

        # Find the number of descendant halos from the size of the unique array
        ndesc = unidesc_haloids.size

        # Assign the corresponding number of particles in each descendant for storing.
        # Could be extracted later from halo data but make analysis faster to save it here.
        # This can be done simply by using the ID of the descendant since again np.unique returns
        # sorted results.
        desc_npart = desc_counts[unidesc_haloids]

        # Sort the halo IDs and number of particles in each progenitor halo by their contribution to the
        # current halo (number of particles from the current halo in the progenitor or descendant)
        sorting_inds = unidesc_counts.argsort()[::-1]
        desc_npart = desc_npart[sorting_inds]
        desc_haloids = unidesc_haloids[sorting_inds]
        desc_mass_contribution = unidesc_counts[sorting_inds]

    # If there are no descendant store Null values
    else:
        ndesc = 0
        desc_npart = np.array([-1], copy=False, dtype=int)
        desc_haloids = np.array([-1], copy=False, dtype=int)
        desc_mass_contribution = np.array([-1], copy=False, dtype=int)

    return nprog, prog_haloids, prog_npart, prog_mass_contribution, ndesc, desc_haloids, desc_npart, desc_mass_contribution


def directProgDescWriter(snapshot, halopath='halo_snapshots/', savepath='MergerGraphs/', part_threshold=10):
    """ A function which cycles through all halos in a snapshot finding and writing out the
    direct progenitor and descendant data.

    :param snapshot: The snapshot ID.
    :param halopath: The filepath to the halo finder HDF5 file.
    :param savepath: The filepath to the directory where the Merger Graph should be written out to.
    :param part_threshold: The mass (number of particles) threshold defining a halo.

    :return: None
    """

    # =============== Current Snapshot ===============

    # Load the current snapshot data
    hdf_current = h5py.File(halopath + 'halos_' + snapshot + '.hdf5', 'r')

    # Extract the halo IDs (group names/keys) contained within this snapshot
    halo_ids = list(hdf_current.keys())
    hdf_current.close()  # close the root group to reduce overhead when looping

    # =============== Progenitor Snapshot ===============

    # Define the progenitor snapshot ID (current ID - 1)
    if int(snapshot) > 10:
        prog_snap = '0' + str(int(snapshot) - 1)
    else:
        prog_snap = '00' + str(int(snapshot) - 1)

    # Only look for progenitor data if there is a progenitor snapshot (subtracting 1 from the
    # earliest snapshot results in a snapshot ID of '00-1')
    if prog_snap != '00-1':

        # Load the progenitor snapshot
        hdf_prog = h5py.File(halopath + 'halos_' + prog_snap + '.hdf5', 'r')

        # Extract the particle halo ID array and particle ID array
        prog_snap_haloIDs = hdf_prog['Halo_IDs'].value
        hdf_prog.close()

        # Get all the unique halo IDs in this snapshot and the number of times they appear
        prog_unique, prog_counts = np.unique(prog_snap_haloIDs, return_counts=True)

        # Make sure all halos at this point in the data have more than ten particles
        assert all(prog_counts >= 10), 'Not all halos are large than the minimum mass threshold'

        # Remove single particle halos (ID=-2), since np.unique returns a sorted array this can be
        # done by removing the first value
        prog_unique = prog_unique[1:]
        prog_counts = prog_counts[1:]

    else:  # Assign an empty array if the snapshot is less than the earliest (000)
        prog_snap_haloIDs = np.array([], copy=False)
        prog_counts = []

    # =============== Descendant Snapshot ===============

    # Define the descendant snapshot ID (current ID + 1)
    if int(snapshot) > 8:
        desc_snap = '0' + str(int(snapshot) + 1)
    else:
        desc_snap = '00' + str(int(snapshot) + 1)

    # Only look for descendant data if there is a descendant snapshot (last snapshot has the ID '061')
    if int(desc_snap) <= 61:

        # Load the descendant snapshot
        hdf_desc = h5py.File(halopath + 'halos_' + desc_snap + '.hdf5', 'r')

        # Extract the particle -> halo ID array and particle ID array
        desc_snap_haloIDs = hdf_desc['Halo_IDs'].value
        hdf_desc.close()

        # Get all unique halos in this snapshot
        desc_unique, desc_counts = np.unique(desc_snap_haloIDs, return_counts=True)
        
        # Make sure all halos at this point in the data have more than ten particles
        assert all(desc_counts >= 10), 'Not all halos are large than the mass threshold'

        # Remove single particle halos (ID=-2), since np.unique returns a sorted array this can be
        # done by removing the first value
        desc_unique = desc_unique[1:]
        desc_counts = desc_counts[1:]

    else:  # Assign an empty array if the snapshot is less than the earliest (061)
        desc_snap_haloIDs = np.array([], copy=False)
        desc_counts = []

    # =============== Find all Direct Progenitors And Descendant Of Halos In This Snapshot ===============

    # Initialise the progress
    progress = -1

    # Assign the number of halos for progress reporting
    size = len(halo_ids)

    # Loop through all the halos in this snapshot
    for num, haloID in enumerate(halo_ids):

        # Print progress
        previous_progress  = progress
        progress = int(num/size * 100)
        if progress != previous_progress:
            print('Graph progress: ', progress, '%', haloID)

        # Ignore HDF5 keys which are not halo's
        if haloID == 'Halo_IDs' or haloID == 'Part_IDs': continue

        # =============== Current Halo ===============

        # Load the snapshot data
        hdf_current = h5py.File(halopath + 'halos_' + snapshot + '.hdf5', 'r')

        # Assign the particle IDs contained in the current halo
        current_halo_pids = hdf_current[str(haloID) + '/Halo_Part_IDs'].value

        hdf_current.close()  # close the root group to reduce overhead when looping

        # If halo falls below the 20 cutoff threshold do not look for progenitors or descendants
        if current_halo_pids.size >= part_threshold:

            # =============== Run The Direct Progenitor and Descendant Finder ===============

            # Run the progenitor/descendant finder

            result = directProgDescFinder(current_halo_pids, prog_snap_haloIDs, desc_snap_haloIDs,
                                          prog_counts, desc_counts, part_threshold)
            (nprog, prog_haloids, prog_npart, prog_mass_contribution,
             ndesc, desc_haloids, desc_npart, desc_mass_contribution) = result

            # =============== Write Out Data ===============

            try:
                # Open the snapshot root group
                hdf = h5py.File(savepath + 'Mgraph_' + snapshot + '.hdf5', 'r+')
            except OSError:
                hdf = h5py.File(savepath + 'Mgraph_' + snapshot + '.hdf5', 'w')

            # Write out the data produced
            halo = hdf.create_group(haloID)  # create halo group
            halo.attrs['nProg'] = nprog  # number of progenitors
            halo.attrs['nDesc'] = ndesc  # number of descendants
            halo.attrs['current_halo_nPart'] = current_halo_pids.size  # mass of the halo
            halo.create_dataset('current_halo_partIDs', data=current_halo_pids, dtype=int)  # particle ids in this halo
            halo.create_dataset('prog_mass_contirbution', data=prog_mass_contribution, dtype=int)  # Mass contribution
            halo.create_dataset('desc_mass_contirbution', data=desc_mass_contribution, dtype=int)  # Mass contribution
            halo.create_dataset('Prog_nPart', data=prog_npart, dtype=int)  # number of particles in each progenitor
            halo.create_dataset('Desc_nPart', data=desc_npart, dtype=int)  # number of particles in each descendant
            halo.create_dataset('Prog_haloIDs', data=prog_haloids, dtype=int)  # progenitor IDs
            halo.create_dataset('Desc_haloIDs', data=desc_haloids, dtype=int)  # descendant IDs
            hdf.close()

    return
